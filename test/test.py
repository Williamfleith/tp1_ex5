# -*- coding: utf-8 -*-
"""
Created on 25/03/2020
by William FLEITH
"""
import sys

# Insert in the first value of the PythonPath list the PYTHONPATH corresponding to this exercise.
sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex5")
from calculator.ex5 import SimpleCalculator

# Main code, differents test to check whether our verification work or not
RES = SimpleCalculator(3, 2)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())

RES = SimpleCalculator(3, 1.2)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())

RES = SimpleCalculator(3.3, 1)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())

RES = SimpleCalculator(3, 0)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())

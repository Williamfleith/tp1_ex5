Readme Ex5 :

Created on 21/04/2020 by William FLEITH

In this exercise we are checking whether or not number are integer and also if we are dividing by 0  in the divide function. 

- To run the programm go to the package test and run test.py
- The line : sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex5") enable not to setup the PYTHONPATH every time, it will do it automatically everytime we will run the programm.
- Becareful to change this line (line 9) on test/test.py with your absolute right way.
- Files with extension .pyc are removed from the repository.
- Init files are only for the initiation of packages so they are likely to be empty.
# -*- coding: utf-8 -*-
"""
Created on 25/03/2020
by William FLEITH
"""


class SimpleCalculator:
    """
    Will calculate the operation of 2 variables
    """

    def __init__(self, init1, init2):
        """
        Init function
        isinstance test if our number are integer or not
        """
        if (isinstance(init1, int) and isinstance(init2, int)) is False:
            raise ValueError("Need integers input")
        self.number1 = init1
        self.number2 = init2

    def sum(self):
        """
        addition function
        """

        print(self.number1, "+", self.number2, "=")
        result = self.number1 + self.number2
        return result

    def substract(self):
        """
        substract function
        """
        print(self.number1, "-", self.number2, "=")
        result = self.number1 - self.number2
        return result

    def multiply(self):
        """
        multiply function
        """
        print(self.number1, "*", self.number2, "=")
        result = self.number1 * self.number2
        return result

    def divide(self):
        """
        divide function
        It also check whether to divide by 0 or not.
        """
        if self.number2 == 0:
            raise ZeroDivisionError("Cannot divide by zero")

        print(self.number1, "/", self.number2, "=")
        result = self.number1 / self.number2
        return result
